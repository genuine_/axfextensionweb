
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    passport = require('passport'),
    flash   = require('connect-flash');

var app = module.exports = express.createServer();


passport.serializeUser(function(user,done) {
    done(null,user);
});

passport.deserializeUser( function(obj, done) {
    done(null,obj);
})

// Configuration

app.configure(function(){
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('blah spat'));
    app.use(express.session({cookie:{maxAge:60000},secret:'axfRoxUrSox'}));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
    app.use(express.errorHandler()); 
});

// Routes

app.get('/', routes.index);
app.get('/list', routes.list);
//app.get('/csv', routes.csv);
app.get('/register', routes.register);

app.post('/reg', routes.reg_post);
app.post('/login', passport.authenticate('local', { successRedirect: '/',
    failureRedirect: '/register',
    failureFlash: true }), routes.login);

// Open App socket
app.listen(3003);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

function requireAuthentication(req, res, next) {
    console.log('req.user: ' + req.user);
    console.log('req.isAuthenticated(): ' + req.isAuthenticated());
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/register');
}