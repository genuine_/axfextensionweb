// loads model file and engine
var mongoose    = require('mongoose'),
    memberModel = require('../models/UserModel'),
    subModel    = require('../models/SubmissionModel');

var passport = require("passport"),
    LocalStrategy = require('passport-local').Strategy;

// Open DB connection
mongoose.connect('mongodb://localhost/axfExt');

// Home page
exports.index = function(req, res){
    res.render('index.jade', { title: 'My Registration App', messages: [], errors: [] });
};

//registration page
exports.register = function(req, res){
    res.render('register.jade', { title: 'Register AXF Author Registration', messages: [], errors: [] });

}

// Extension list page
exports.list = function(req, res){
    memberModel.find({},function(err, docs){
        //console.log("docs returned: " + docs);
        res.render('list.jade', { title: 'My Registration App - Member list', members: docs });
    });
};


exports.login = function(req, res){
    passport.use(new LocalStrategy(
        function(username, password, done)
        {
            //find the username
            memberModel.findOne({username:username}, function(err, user)
            {
                if(err) { return done(err); }
                if(!user)
                {
                    return done(null,false,{message:'Incorrect username entered.'});
                }
                //check password hash
                if(!user.comparePassword(password,function(err,isMatch){
                    if(err) {
                        console.log("Error in password verfification: "+ err);
                        throw err;
                    }
                }
                )
                    )
                {
                    return done(null, false, {message:'Incorrect passsword.'});
                }
                return done(null,user);
            });
        }
    ));
};


// Member register logic
exports.reg_post = function(req, res){
    member = new memberModel();
    member.username = req.body.username;
    member.password = req.body.password;
    member.email = req.body.email;
    member.trust = 0;
    member.date_joined = new Date();
    console.log(member.date_joined);
    
    member.save(function (err) {
        messages = [];
        errors = [];
        if (!err){
            console.log('Success!');
            //console.log(member.username + " " + member.password);
            messages.push("Thank you for your new membership !");
        }
        else {
            console.log('Error !');
            errors.push("At least a mandatory field has not passed validation...");
            console.log(err);
        }
        res.render('register.jade', { title: 'My Registration App', messages: messages, errors: errors });
    });
};