/**
 * Created with JetBrains WebStorm.
 * User: glindor
 * Date: 4/26/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var mongooseTypes = require("mongoose-types");
mongooseTypes.loadTypes(mongoose);

//var mail = new Schema({email:Email});



var UserSchema = new Schema({
    id      : ObjectId,
    username: { type:String, required:true, index:{ unique:true,
        sparse:true } },
    password: { type:String, required:true },
    email   : { type:String, required:true, index:{ unique:true,
        sparse:true } },
    banned  : {type:Boolean, default:false },
    trust   : { type:Number, required:true, default: 0 },
    date_joined: Date
});



UserSchema.pre('save', function(next, done) {
    var user = this;
// only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

// generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            console.log("HASH: "+ user.password );
            next();
        });
    });
});


//find a user
UserSchema.methods.findUser = function(name, callback){
    return this.findOne({username: name}, callback(err,user));
}

UserSchema.methods.comparePassword = function(incomingpass, callback) {
    bcrypt.compare(incomingpass, this.password, function(err, isMatch) {
        if(err) return next(err);

        callback(null, isMatch);
    });
};

/*Date setter
UserSchema.path('date')
    .default(function(){
        return new Date()
    })
    .set(function(v){
        return v == 'now' ? new Date() : v;
    });
*/
module.exports = mongoose.model('Users', UserSchema);

