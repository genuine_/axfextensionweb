/**
 * Created with JetBrains WebStorm.
 * User: glindor
 * Date: 4/26/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserSchema = require("../models/UserModel");


var Submission = new Schema({
    user_id: [UserSchema.username],
    url:    {type: String},
    jsonDesc:   {type: String}
});


//function to return jSON in submission Schema
Submission.methods.getJSON = function(callback){
    var j = this.jsonDesc.toJSON().stringify();
    console.log(j);
    callback(j);
    //return j;
}

module.exports = mongoose.model('Submission', Submission);
